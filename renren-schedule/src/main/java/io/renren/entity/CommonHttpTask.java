package io.renren.entity;

import com.alibaba.fastjson.JSONObject;
import org.aijs.core.web.HttpRequestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Created by aijinsong on 2017/6/18.
 */
@Component("commonHttpTask")
public class CommonHttpTask {

    private static Logger logger = LoggerFactory.getLogger(CommonHttpTask.class);

    public void httpInvoke(String params) {
        JSONObject jsonObject = JSONObject.parseObject(params);
        String url = jsonObject.getString("url");
        String data = jsonObject.getString("data");
        String method = jsonObject.getString("method");
        String protocol = jsonObject.getString("protocol");
        if ("https".equals(protocol)) {
            if ("get".equals(method) || "GET".equals(method)) {
//                HttpRequestUtils.get(url, data);
            }
            if ("post".equals(method) || "POST".equals(method)) {
//                HttpRequestUtils.post(url, data);
            }
        }
        if ("get".equals(method) || "GET".equals(method)) {
            HttpRequestUtils.get(url, data);
            return;
        }
        if ("post".equals(method) || "POST".equals(method)) {
            HttpRequestUtils.post(url, data);
            return;
        }

    }

    public void httpsInvoke() {

    }

}
